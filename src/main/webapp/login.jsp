<%-- 
    Document   : login.jsp
    Created on : Dec 1, 2022, 11:48:39 AM
    Author     : thucl
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Login Page</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    </head>
    <body>
        <form action="MainController" method = "POST">
            UserID<input "type ="text" name= "userID" required/> </br>
            Password<input "type ="password" name= "password" required=""/></br>
            <input "type ="submit" name= "action" value="login"/>
            <input "type ="reset"value="Reset"/>
        </form>
         <% 
            String error = (String) request.getAttribute("ERROR");
            if(error == null){
                error = "";
            }
        %>
        <h1> <%= error %></h1>
    </body>
</html>
