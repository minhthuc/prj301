/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.prj301;

import com.thuc.utils.DBUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author thucl
 */
public class UserDAO {

    private final String LOGIN = "SELECT fullName, rollID FROM tblUsers WHERE userId = ? AND password = ?";

    public UserDTO checkLogin(String userID, String password) throws SQLException, ClassNotFoundException {

        UserDTO loginUser = null;
        Connection conn = null;
        PreparedStatement ps = null;
        ResultSet rs = null;

        try {
            conn = DBUtils.getConnection();
            if(conn != null){
                ps = conn.prepareStatement(LOGIN);
                ps.setString(1, userID);
                ps.setString(2, password);
                rs = ps.executeQuery();
                while(rs.next()){
                    String fullName = rs.getString("fullName");
                    String rollId = rs.getString("roleId");
                    loginUser = new UserDTO(userID, fullName, rollId, password);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.toString());
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (ps != null) {
                ps.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
        return loginUser;
    }
}
