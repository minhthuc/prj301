/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.prj301;

import java.util.logging.Logger;

/**
 *
 * @author thucl
 */
public class UserDTO {

    private String userId, fullName, roll, password;
    private static final Logger LOG = Logger.getLogger(UserDTO.class.getName());

    public UserDTO() {
        this.userId = "";
        this.fullName = "";
        this.roll = "";
        this.password = "";
    }

    public UserDTO(String userId, String fullName, String roll, String password) {
        this.userId = userId;
        this.fullName = fullName;
        this.roll = roll;
        this.password = password;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getRoll() {
        return roll;
    }

    public void setRoll(String roll) {
        this.roll = roll;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

}
